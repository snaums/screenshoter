#!/bin/python3
from subprocess import *
from datetime import datetime
import time

import sys
import re

import args

def shootMain ():
    cfg = args.parseArguments ( sys.argv );
    if ( cfg.windowname == "" ):
        print ("No window name given!");
        return;
        
    windowPos = findWindow ( cfg.windowname );

    if (cfg.mode == "framegrab" and cfg.refresh == False):
        ffmpeg_grabFrames ( cfg, windowPos[0], windowPos[1], windowPos[2], windowPos[3] );
    elif ( cfg.mode == "framegrab" and cfg.refresh == True):
        while 1:
            windowPos = findWindow ( cfg.windowname );
            ffmpeg_grabSingleFrame ( cfg, windowPos[0], windowPos[1], windowPos[2], windowPos[3] );
            time.sleep ( 1.0 / cfg.fr );
    elif ( cfg.mode == "record"):
        ffmpeg_recordVideo ( cfg, windowPos[0], windowPos[1], windowPos[2], windowPos[3] );
    else : 
        print ("Mode Problem!");
    
    
def filename ( cfg ):
    dt = datetime.now();
    if ( cfg.refresh == False ):
        return "%s/%s_%d_%d_%d-%d_%d_%d-%%04d.png"%(cfg.destDir, cfg.filePrefix, dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second);
    else:
        cfg.frameNumber = cfg.frameNumber + 1;
        return "%s/%s_%d_%d_%d-%d_%d_%d-%d.png"%(cfg.destDir, cfg.filePrefix, dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second, cfg.frameNumber);

## grab frames of the window content with ffmpeg
def ffmpeg_grabFrames ( cfg, x, y, width, height ):
    outfile = filename ( cfg )
    run ( ["ffmpeg","-video_size", "%sx%s"%(width, height), "-framerate",  "%f"%cfg.fr, "-f", "x11grab", "-i", ":0.0+%s,%s"%(x, y), outfile] )

def ffmpeg_grabSingleFrame ( cfg, x, y, width, height ):
    outfile =  filename ( cfg )
    run ( ["ffmpeg","-video_size", "%sx%s"%(width, height), "-f", "x11grab", "-i", ":0.0+%s,%s"%(x, y), "-frames:v", "1", outfile] )

def ffmpeg_recordVideo ( cfg, x, y, width, height ):
    dt = datetime.now();
    outfile = "%s/%s_%d_%d_%d-%d_%d_%d.mkv"%(cfg.destDir, cfg.filePrefix, dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second);
    if (cfg.withaudio):
        if ( len(cfg.audio) > 0 ):
            audio = cfg.audio
        else:
            audio = "default"
        run ( ["ffmpeg","-video_size", "%sx%s"%(width, height), "-framerate",  "%f"%cfg.fr, "-f", "x11grab", "-i", ":0.0+%s,%s"%(x, y), 
               "-f", "pulse", "-ac", "2", "-i", audio, 
               "-c:v", cfg.vcodec, "-b:v", cfg.vb,  
               "-c:a", cfg.acodec, "-b:a", cfg.ab,
               outfile] )
    else:
        run ( ["ffmpeg","-video_size", "%sx%s"%(width, height), "-framerate",  "%f"%cfg.fr, "-f", "x11grab", "-i", ":0.0+%s,%s"%(x, y), 
               "-c:v", cfg.vcodec, "-b:v", cfg.vb, 
               outfile] )

## find the window using the xwinfo program
def findWindow ( windowname ):
    p = Popen ( ['xwininfo', '-tree', '-root'], stdout=PIPE)

    hwnd = p.communicate()[0]
    hwnd = hwnd.decode ( "utf-8" )

    wname = re.compile ( windowname );
    inrgx = re.compile ( "0x[0-9a-f]+? *\"(.+?)\": *\(.+\) *([0-9]+)x([0-9]+)\+[0-9]+\+[0-9]+ *\+([0-9]+)\+([0-9]+)")
    h = hwnd.split("\n");
    out = [-1,-1,-1,-1]
    # iterate over the lines
    for hl in h:
        res = inrgx.search ( hl )
        if ( res != None ):
            r = wname.search ( res.group(1) )
            if ( r != None ):
                print ( "%s -- %s %s ---- %s %s\n"%(res.group(1),res.group(2),res.group(3),res.group(4),res.group(5)))
                #break

                out = [res.group(4),res.group(5),res.group(2),res.group(3)]
                break

    return out

if __name__ == "__main__" : 
    shootMain()
