import sys

class Config:
    parameter = {
        "-h"            : "lists all available options",
        "--help"        : "lists all available options",
        
        "--grab"        : "take framegrabs (enable screenshot mode)",        
        "--refresh"     : "before taking a new screenshot find the window again",
        "--record"      : "take video (enable video mode)",
        "--with-audio"  : "activate audio when recording video. enable record-mode",

        
        "--prefix"      : "prefix of the recorded files, e.g. the name of the application",
        "--destination" : "where should the files be stored",
        "--window"      : "name of the window or parts of the name",
        
        "-fr"           : "framerate, i.e. 25 -> 25 frames per second",
        "-framerate"    : "framerate, i.e. 25 -> 25 frames per second",
        "--delay"       : "time _per_ frame, i.e.  2 means 1 frame every 2 seconds",
        
        "-vocdec"       : "ffmpeg video codec, remember to enable record-mode",
        "-c:v"          : "ffmpeg video codec, remember to enable record-mode", 
        "-acodec"       : "ffmpeg audio codec, remember to enable audio and record-mode",
        "-c:a"          : "ffmpeg audio codec, remember to enable audio and record-mode",
        "-vb"           : "video bitrate",
        "-b:v"          : "video bitrate",
        "-ab"           : "audio bitrate",
        "-b:a"          : "audio bitrate",
        "-preset"       : "x264 preset (if x264 is used)",
    }
    
    def __init__ ( self ):    
        self.mode="framegrab"
        self.fr = 1.0/20.0 # one image per 20 seconds
        self.destDir = "."
        self.filePrefix = "default"
        self.refresh = False
        self.frameNumber = 0
        self.windowname = ""
        self.verbose = ""
        self.audio = ""
        self.withaudio = False
        self.vcodec  = "x264"
        self.vb = "5000k"
        self.preset = "medium"
        self.acodec = "vorbis"
        self.ab = "320k"
        
    def __str__ ( self ):
        result = ""
        for a in dir(self):
            if ( type(getattr(self,a)) is int ):
                result = result + "%s = %d\n"%(a, getattr(self,a))
            elif ( type(getattr(self,a)) is str ):
                result = result + "%s = %s\n"%(a, getattr(self,a))
            elif ( callable(getattr(self,a)) ):
                pass
            else:
                result = result + "%s = %s\n"%(a, getattr(self, a))
        return (result) 
    
    def help ( self ):
        print ("You are using Oettinger Games Screenshoter.\nIt takes screenshots every X seconds.\n\nUsage:\n    python shoot.py [options]\n\nExample (X=42): \n    python shoot.py --window \"Terminal\" --grab -fr 42\n\nOptions:")
        lastVal = ""
        for key in self.parameter:
            skey = key;
            if ( key[0]== "-" and key[1] != "-" ):
                skey = " {}".format(key)
            if lastVal != self.parameter[key]:
                print (" {:13} -- {}".format(skey,self.parameter[key]))
            else:
                print (" {:13}".format(skey))
            lastVal = self.parameter[key]
                
def parseArguments ( args ):
    cfg = Config();
    state =""
    for argv in args:
        # skip the first argument (scriptname)
        if ( argv == sys.argv[0] ):
            pass
        elif ( argv in cfg.parameter and state != argv ):
            state = argv
            if ( state == "--record" ):
                cfg.mode = "record"
            elif ( state == "--with-audio" ):
                cfg.withaudio = True
            elif ( state == "--grab" ):
                cfg.mode = "framegrab";
            elif ( state == "-h" or state == "--help" ):
                cfg.help()
                sys.exit(0);
            elif ( state == "--refresh" ):
                cfg.refresh = True
        elif ( state == "-fr" ):
            cfg.fr = float(argv)
        elif ( state == "--delay"):
            cfg.fr = 1.0 / float(argv);
        elif ( state == "--prefix" ):
            cfg.filePrefix = argv
        elif ( state == "--destination" ):
            cfg.destDir = argv
        elif ( state == "--window" ):
            cfg.windowname = argv
        elif ( state == "-vcodec" or state == "-c:v" ):
            cfg.vcodec = argv
        elif ( state == "-vb" or state == "-b:v"):
            cfg.vb = argv
        elif ( state == "-preset" ):
            cfg.preset = argv
        elif ( state == "-acodec" or state == "-c:a"):
            cfg.acodec = argv
        elif ( state == "-ab" or state == "-b:a"):
            cfg.ab = argv
    return cfg;
