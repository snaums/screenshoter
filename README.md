# screenshoter

Script screenshoting the contents of a window every X seconds based on ffmpeg.

## What it does

Identifies the window (X,Y,Width, height) and instructs ffmpeg to take frame grabs from the window's contents. Can also record video using ffmpeg. 

## Requirements

- ffmpeg
- xwininfo

## Usage

```
python shoot.py [options]
```

Most options take an additional parameter. The order of options is of no concerns, however the last one will take precedence if you decide to give an option several times.
```
python shoot.py --window "wine" --prefix "Anno1602" --destination "Screen" --grab
```

Allowed options are:
```
-h | --help      -- lists all available options (no comments on them though)

# screenshoter's mode
--grab           -- take framegrabs
--record         -- take video
--with-audio     -- activate audio when recording video. Makes only sence with --record
--refresh        -- before taking a new screenshot - find the position of the window again. 

# destination and window
--prefix         -- prefix of the recorded files. May be the name of the game / application
--destination    -- where should the files be stored?
--window         -- name of the window

# framerate
--fr             -- framerate, i.e. 25 -> 25 frames per second
--delay          -- time _per_ frame, i.e.  2 means 1 frame every 2 seconds

# ffmpeg options
-vocdec | -c:v   -- which video codec - see ffmpeg's lists of codecs, remember to enable record-mode
-acodec | -c:a   -- which audio codec - see ffmpeg's lists of codecs, remember to enable audio, enable record-mode
-vb              -- video bitrate
-ab              -- audio bitrate
-preset          -- x264 preset (if x264 is used)
```


